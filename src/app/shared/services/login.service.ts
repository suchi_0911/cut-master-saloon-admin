import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  loginUser(payload: any) {
    const url = `/user/loginUser`;
    return this.http.post(url, payload);
  }
}
