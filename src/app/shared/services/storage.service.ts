import { Injectable } from '@angular/core';
import { AES, enc } from 'crypto-js';

const SECRET_KEY = 'test-key';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  encryptData(value: any): any {
    return AES.encrypt(JSON.stringify(value), SECRET_KEY).toString();
  }

  decryptData(value: any): any {
    try {
      if (value !== null && value !== '') {
        const bytes: any = AES.decrypt(value.toString(), SECRET_KEY);
        return JSON.parse(bytes.toString(enc.Utf8));
      }
      else {
        return value;
      }
    } catch (error) {
      console.log(error);
      sessionStorage.clear();
    }
  }

  getItem(key: string): any {
    const data = sessionStorage.getItem(key);
    return this.decryptData(data);
  }

  setItem(key: string, value: any): void {
    sessionStorage.setItem(key, this.encryptData(value));
  }

  removeItem(key: string): void {
    sessionStorage.removeItem(key);
  }

  removeAll(): void {
    sessionStorage.clear();
  }
}
