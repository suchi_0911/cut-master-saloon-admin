import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
    HttpHeaders,
    HttpErrorResponse,
    HttpEvent
} from '@angular/common/http';
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
@Injectable()
export class HttpInterceptorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url: string = environment.apiUrl + req.url;
        const headersObj: any = '';

        return next.handle(req.clone({
            url,
            headers: new HttpHeaders(headersObj)
        }))
    }
}
