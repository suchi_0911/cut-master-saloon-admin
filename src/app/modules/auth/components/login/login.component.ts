import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/services/login.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});
  isHide : boolean = false;
  constructor(private loginService: LoginService, private storage: StorageService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    })
  }

  onLogin(): void {
    this.loginService.loginUser(this.loginForm.value).toPromise().then((res: any) => {
      const { data: { auth_token, user : { role } }, user } = res;
      this.storage.setItem('role', role)
      this.storage.setItem('authToken', auth_token);
      this.storage.setItem('userDetails', user);
      this.router.navigate(['/dashboard'])
    }).catch((error) => {
      console.log(error);
    })
  }

}
